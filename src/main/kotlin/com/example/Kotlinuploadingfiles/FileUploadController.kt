package com.example.Kotlinuploadingfiles

import com.example.Kotlinuploadingfiles.storage.StorageFileNotFoundException
import com.example.Kotlinuploadingfiles.storage.StorageService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.io.Resource
import org.springframework.http.HttpHeaders
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder
import org.springframework.web.servlet.mvc.support.RedirectAttributes
import java.io.IOException
import java.util.stream.Collectors


@Controller
class FileUploadController @Autowired constructor(storageService: StorageService) {
    private val storageService: StorageService

    init {
        this.storageService = storageService
    }

    @GetMapping("/")
    @Throws(IOException::class)
    fun listUploadedFiles(model: Model): String {
        model.addAttribute("files", storageService.loadAll()?.map { path ->
            MvcUriComponentsBuilder.fromMethodName(
                FileUploadController::class.java,
                "serveFile", path?.fileName.toString()
            ).build().toUri().toString()
        }
            ?.collect(Collectors.toList()))
        return "uploadForm"
    }

    @GetMapping("/files/{filename:.+}")
    @ResponseBody
    fun serveFile(@PathVariable filename: String?): ResponseEntity<Resource> {
        val file: Resource? = storageService.loadAsResource(filename)
        if (file != null) {
            return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename().toString() + "\"")
                .body<Resource>(file)
        }
        return ResponseEntity.badRequest().build()
    }

    @PostMapping("/")
    fun handleFileUpload(
        @RequestParam("file") files: Collection<MultipartFile>,
        redirectAttributes: RedirectAttributes
    ): String {


        files.forEach {
            println(it.originalFilename)
            storageService.store(it)
            redirectAttributes.addFlashAttribute(
                "message",
                "You successfully uploaded " + it.originalFilename + "!"
            )
        }

        return "redirect:/"
    }

    @ExceptionHandler(StorageFileNotFoundException::class)
    fun handleStorageFileNotFound(exc: StorageFileNotFoundException?): ResponseEntity<*> {
        return ResponseEntity.notFound().build<Any>()
    }


}